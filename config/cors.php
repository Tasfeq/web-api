<?php
return [
    /*
     |--------------------------------------------------------------------------
     | Laravel CORS
     |--------------------------------------------------------------------------
     |
     | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
     | to accept any value.
     |
     */
    'supportsCredentials' => true,
    'allowedOrigins' => ['*'],
    'allowedHeaders' => ['*'],
    'allowedMethods' => ['GET', 'POST', 'PUT',  'DELETE',  'OPTIONS', 'PATCH'],
    'exposedHeaders' => ['Authorization','ACCESS-CONTROL-ALLOW-ORIGIN'],
    'maxAge' => 0,
    'hosts' => [],
];
