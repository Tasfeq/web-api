<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Log;

class Inspire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inspire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mobile = ['01686311877','01679007989'];
        $message = 'Medicine time alert !!!!';
        $origin = '01686311877';
        foreach ($mobile as $item){
            $this->senderSpecificSendSMS($item,$message,$origin);
        }

        $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
    }


    protected function senderSpecificSendSMS($number,$message,$originator) {
        $url = 'http://smartsms.co/api/send?';
        $api_key = 'd7bc1ebd188323e21a2539d4dde4d054';
        $sender = "GHOORI";
        Log::info('in sms');
        $message = urlencode($message);

        $requestUrl = $url.
            "api_key=$api_key".
            "&contact=$number".
            "&sender=$sender".
            "&sms=$message";
        try {
            $response = $this->SendNow($requestUrl);
        } catch (Exception $e) {
            $response = false;
        }
        return $response;
    }
    protected function SendNow($requestUrl) {
        $response = @file_get_contents($requestUrl);
        if ($response === FALSE) {
            throw new Exception("Cannot access.");
        } else {
            return $response;
        }
    }
    
}
