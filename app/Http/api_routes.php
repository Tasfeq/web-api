<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware'=>'cors'],function ($api) {

    $api->get('test/{token}',['uses'=>'App\Api\V1\Controllers\AuthController@getUserInfoFromToken']);

    $api->post('auth/login', ['as' => 'login', 'uses' => 'App\Api\V1\Controllers\AuthController@login']);
    $api->post('auth/signup', [ 'uses' => 'App\Api\V1\Controllers\AuthController@signup']);
    $api->post('auth/recovery', 'App\Api\V1\Controllers\AuthController@recovery');
    $api->post('auth/reset', 'App\Api\V1\Controllers\AuthController@reset');
    $api->get('auth/logout',['middleware'=>['jwt.auth'], 'as'=>'logout', 'uses'=>'App\Api\V1\Controllers\AuthController@logout']);
    $api->post('auth/fb/login',['uses'=>'App\Api\V1\Controllers\AuthController@postFbLogin']);


    $api->post('auth/account-kit-login',['as'=>'accountkit.login','uses' => 'App\Api\V1\Controllers\AuthController@postFbAccountKitLogin']);
    
    //get user info by jwt token
    $api->get('auth/userinfo', ['middleware' => [ 'jwt.refresh'], 'as' => 'userinfo', 'uses' => 'App\Api\V1\Controllers\AuthController@getUserInfo']);
    $api->get('auth/refresh', ['middleware' => ['api.auth', 'jwt.refresh'], 'as' => 'refreshToken', 'uses' => 'App\Api\V1\Controllers\AuthController@refreshToken']);
    $api->put('users/reset', ['middleware' => ['api.auth', 'jwt.refresh'], 'as' => 'users.reset', 'uses' => 'App\Api\V1\Controllers\AuthController@postResetPassword']);

    $api->get('password/email', 'App\Api\V1\Controllers\PasswordController@getEmail');
    $api->post('password/email', 'App\Api\V1\Controllers\PasswordController@postPasswordResetEmail');

    // Password reset routes...
    $api->get('password/reset/{token}', 'App\Api\V1\Controllers\PasswordController@getReset');
    $api->post('password/reset', 'App\Api\V1\Controllers\PasswordController@postReset');

    // Person details route
    $api->get('person/{id}',['middleware'=>'cors','uses'=>'App\Api\V1\Controllers\PersonController@show']);

    //wordpress admin api
    $api->get('wp-admin/elastic-search-data/{videoId}', 'App\Api\V1\Controllers\ElasticSearchController@wp_videoData');
    //Search api
    $api->get('search',['uses'=>'App\Api\V1\Controllers\ElasticSearchController@getSearchResult']);

    // Video details route
    $api->get('video/{id}',['uses'=>'App\Api\V1\Controllers\VideoController@show']);
    $api->get('video/slug/{slug}',['uses'=>'App\Api\V1\Controllers\VideoController@showBySlug']);
    
    //similar videos @todo: clear it
    $api->get('video/similar/{id}',['uses'=>'App\Api\V1\Controllers\VideoController@getSimilarVideos']);

    //recommended video @todo: clear it
    $api->get('video/recommended/{id}',['uses'=>'App\Api\V1\Controllers\VideoController@getRecommendedVideos']);
    //recommended video for home page
    $api->get('recommended-video',['uses'=>'App\Api\V1\Controllers\VideoController@getRecommendedVideos']);
    //videoActionLog
    $api->post('video/{action}/{videoId}',['uses'=>'App\Api\V1\Controllers\VideoController@videoActionLog']);
    //get video comment by video id
    $api->get('video-comment/{id}',['uses'=>'App\Api\V1\Controllers\CommentController@getVideoComment']);
    //authentication
    //
    $api->group(['middleware' => ['jwt.refresh','jwt.auth']], function ($api) {

        $api->get('auth/userinfo',['as'=>'userinfo','uses'=>'App\Api\V1\Controllers\AuthController@getUserInfo']);
        $api->post('auth/userinfo',['uses'=>'App\Api\V1\Controllers\AuthController@updateUserInfo']);
        //test @todo: clear it
        $api->get('test1',['uses'=>'App\Api\V1\Controllers\VideoController@user']);
        //video like api
        $api->get('video-like/{id}',['uses'=>'App\Api\V1\Controllers\VideoController@videoLike']);
        //Video comment api
        $api->post('video-comment/{videoId}',['uses'=>'App\Api\V1\Controllers\CommentController@postVideoComment']);
        $api->post('video-comment/update/{commentId}',['uses'=>'App\Api\V1\Controllers\CommentController@updateVideoComment']);
        $api->get('video-comment/delete/{commentId}',['uses'=>'App\Api\V1\Controllers\CommentController@deleteVideoComment']);
    });

    //category api
    $api->get('category/{categoryName}',['uses'=>'App\Api\V1\Controllers\VideoController@categoryVideos']);
    //recently uploaded video api @todo: clear it
    $api->get('recent-video',['uses'=>'App\Api\V1\Controllers\VideoController@recentVideos']);
    //popular drama api @todo: clear it
    $api->get('popular-drama',['uses'=>'App\Api\V1\Controllers\VideoController@popularDrama']);
    //popular music @todo: clear it
    $api->get('popular-music',['uses'=>'App\Api\V1\Controllers\VideoController@popularMusic']);
    //trending video
    $api->get('trending-video',['uses'=>'App\Api\V1\Controllers\VideoController@getTrendingVideo']);
    //recent movie @todo: clear it
    $api->get('recent-movie',['uses'=>'App\Api\V1\Controllers\VideoController@getRecentMovies']);
    //recent drama @todo: clear it
    $api->get('recent-drama',['uses'=>'App\Api\V1\Controllers\VideoController@getRecentDrama']);
    //drama serial getDramaSerials 
    $api->get('drama-serials',['uses'=>'App\Api\V1\Controllers\VideoController@getDramaSerials']);
    //Similar Video api 
    $api->get('similar-video/{videoId}',['uses'=>'App\Api\V1\Controllers\VideoController@similarVideos']);
    $api->get('similar-video/slug/{slug}',['uses'=>'App\Api\V1\Controllers\VideoController@similarVideosBySlug']);
    //General Recent Videos By Category Api
    $api->get('recent/{categoryName}',['uses'=>'App\Api\V1\Controllers\VideoController@getRecentVideosByCategoryName']);
    //General Popular Videos By Category Api
    $api->get('popular/{categoryName}',['uses'=>'App\Api\V1\Controllers\VideoController@getPopularVideosByCategoryName']);
    //CategoryId and CategoryName Common Api
    $api->get('general-category',['uses'=>'App\Api\V1\Controllers\VideoController@getVideosByCategoryIdorCategoryNameorCategorySlug']);
    //Category tree Api
    $api->get('category-tree',['uses'=>'App\Api\V1\Controllers\CategoryController@getVideoCategoryTree']);
    //Homepage Banner Api
    $api->get('homepage-banner',['uses'=>'App\Api\V1\Controllers\VideoController@getHomepageBanners']);
    //video slug script
    $api->get('insert-video-slugs',['uses'=>'App\Api\V1\Controllers\VideoController@setVideoSlugs']);
    //category slug script
    $api->get('insert-category-slugs',['uses'=>'App\Api\V1\Controllers\CategoryController@setCategorySlug']);
    //bulk video json data for Elastic Search Indexing
    $api->get('bulk-video-json',['uses'=>'App\Api\V1\Controllers\VideoController@bulkVideoJson']);
});
