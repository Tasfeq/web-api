<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VideoMetadata extends Model
{
    protected $table = 'video_metadata';
}
