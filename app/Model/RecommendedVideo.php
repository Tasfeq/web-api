<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RecommendedVideo extends Model
{
    protected $table = 'recommended_videos';
}
