<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VideoWatchLog extends Model
{
    protected $table = 'video_watch_logs';
    protected $fillable = ['video_id','user_id','action','time','browser','browser_version','screen_size',
                            'mobile','platform','mobile','flash','os','os_version','cookie'];
}
