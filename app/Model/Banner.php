<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';

    public function videos(){
        return $this->belongsTo('App\Model\Video','video_id','id');
    }
}
