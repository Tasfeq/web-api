<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralPlaylist extends Model
{
    protected $table = "general_playlists";
}
