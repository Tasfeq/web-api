<?php namespace App\Api\V1\Repo;

use App\Model\GeneralPlaylist;
use App\Model\UserPlaylist;
use App\Model\Video;
use App\Model\VideoWatchLog;
use Illuminate\Support\Facades\DB;

class VideoRepo{

    /**
     * @var Video
     */
    private $model;

    public function __construct(Video $model)
    {
        $this->model = $model;
    }


    public function getVideoDataById($id)
    {

        $data = $this->model->with('persons','productionHouse','categories')->where('videos.id', $id)->first();
        $result = $this->customizePersonDetailsWithType($data->toArray());
        return $result;
    }

    public function getVideoBySlug($slug){
        $data = $this->model->with('persons','productionHouse','categories')->where('videos.slug', $slug)->first();
        $result = $this->customizePersonDetailsWithType($data->toArray());
        return $result;
    }
    
    public function getVideoDetails($id,$user_id){
        $res = $this->model
                ->with(['productionHouse','persons','categories','generalPlaylists'=>function($q) use($id){
                    $q->where('general_playlists.video_id',$id);
                },'users'=>function($q) use($user_id){
                        $q->where('users.id',$user_id);
                    }])
//                ->whereHas('generalPlaylists',function ($q) use($id){
//                    $q->where('general_playlists.video_id',$id);
//                })
                ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
                ->find($id);
        return $res?$this->customizePersonDetailsWithType($res->toArray()):null;
    }

    public function getVideoDetailsBySlug($slug,$user_id){
        $res = $this->model
            ->with(['productionHouse','persons','categories','videoMetadata',
//                'generalPlaylists'=>function($q) use($id){
//                    $q->where('general_playlists.video_id',$id);
//                },
                'users'=>function($q) use($user_id){
                $q->where('users.id',$user_id);
            }])
//                ->whereHas('generalPlaylists',function ($q) use($id){
//                    $q->where('general_playlists.video_id',$id);
//                })
            ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
            ->where('slug',$slug)
            ->first();
        $res = $res->load([
                    'generalPlaylists'=>function($q) use($res){
                        $q->where('general_playlists.video_id',$res->id);
                    }
                ]);
        return $res?$this->customizePersonDetailsWithType($res->toArray()):null;
    }

    private function customizePersonDetailsWithType($data){
        $persons = $data['persons'];
        unset($data['persons']);
        $customizedData = array();
        foreach ($persons as $person){
            $index =  !empty($customizedData[$person['pivot']['role']])?count($customizedData[$person['pivot']['role']]):0;
            $customizedData[$person['pivot']['role']][$index] = $person;
        }
        krsort($customizedData);
        $defaultArray['artist'] = null;
        $data['role'] = count($customizedData)?$customizedData:$defaultArray;
        return $data;
    }

    public function insertIntoGeneralPlaylist($videoId){
        $generalPlaylist = new GeneralPlaylist();
        $generalPlaylist->video_id = $videoId;
        $generalPlaylist->save();
    }

    public function insertIntoVideoWatchLog($data){
        VideoWatchLog::create($data);
    }

    public function insertIntoUserPlaylist($videoId,$userId,$inc,$time=null){

        $user = UserPlaylist::where('video_id',$videoId)
                                    ->where('user_id',$userId)
                                    ->first();
        if($user){
            $user->update([
                'count'=>$user->count+$inc,
                'watched'=>($time)
            ]);
        }else{
            UserPlaylist::create([
                'video_id'=>$videoId,
                'user_id'=>$userId,
                'count'=>1,
                'watched'=>$time
            ]);
        }
    }

    public function getSimilarVideos($id){
        $data = Video::with(['persons'])
                    ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
                    ->leftJoin(DB::raw('(SELECT general_playlists.video_id,COUNT(*) AS view
                                    FROM `general_playlists`
                                    GROUP BY general_playlists.`video_id`) as views'),'views.video_id','=','videos.id')
                    ->where('videos.id','!=',$id)
                    ->orderBy('videos.id','desc')
                    ->take(7)
                    ->get();
        return $data;
    }

    public function getRecommendedVideos($id=null)
    {
        $query = Video::with(['persons','recommendedVideo'])
            ->whereHas('recommendedVideo',function ($q){
                $q->where('recommended_videos.active','=',1);
            })
            ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
            ->leftJoin(DB::raw('(SELECT general_playlists.video_id,COUNT(*) AS view
                                    FROM `general_playlists` 
                                    GROUP BY general_playlists.`video_id`) as views'),'views.video_id','=','videos.id');
            if($id){
                $query = $query->where('videos.id','!=',$id);
            }
            $data = $query->take(10)->get();

        return $data;
    }

    public function getRecommendedVideoForHomePage(){
        $data = Video::take(10)->get();
        return $data;
    }



    public function getRecentlyUploadedVideos(){

        $data=$this->model
            ->with('categories')
            ->whereHas('categories',function($q) {
                $q->where('categories.name','!=','Movie');
            })
            ->orderBy('id','DESC')
            ->take(10)
            ->get();
        return $data;
    }

    public function getRecentMovies(){
        return $this->model->with(['categories','galleries'])
            ->whereHas('categories',function($q) {
                $q->where('categories.name','=','Movie');
            })
            ->take(10)
            ->orderBy('created_at','desc')
            ->get();
    }

    public function getPopularDrama(){

        $data = $this->model->with(['persons','productionHouse','categories'])
            ->whereHas('categories',function($q){
                $q->where('category_video.category_id','=',33);
            })
            ->leftJoin(DB::raw('(SELECT `general_playlists`.`video_id`,COUNT(*) AS popular
                                    FROM `general_playlists`
                                    GROUP BY general_playlists.`video_id` ORDER BY popular DESC) as popular'),'popular.video_id','=','videos.id')
            ->orderBy('popular','desc')
            ->take(10)
            ->get();

        return $data;

    }

    public function getPopularMusic(){
        $data = $this->model->with(['persons','productionHouse','categories'])
            ->whereHas('categories',function($q){
                $q->where('category_video.category_id','=',46);
            })
            ->leftJoin(DB::raw('(SELECT `general_playlists`.`video_id`,COUNT(*) AS popular
                                    FROM `general_playlists`
                                    GROUP BY general_playlists.`video_id` ORDER BY popular DESC) as popular'),'popular.video_id','=','videos.id')
            ->orderBy('popular','desc')
            ->take(10)
            ->get();
        return $data;
    }

    public function getTrendingVideo(){
        $data = $this->model->with('trendingVideo')
                    ->whereHas('trendingVideo',function ($q){
                        $q->where('trending_videos.status',"=","Active");
                    })
                    ->orderBy('videos.id','desc')
                    ->get();
        return $data;
    }

    public function getRecentVideoDataByCategory($categoryId){
        $data = $this->model->with('categories')
                    ->whereHas('categories',function($q) use($categoryId){
                        $q->where('category_video.category_id','=',$categoryId);
                    })
                    ->take(3)
                    ->orderBy('created_at','desc')
                    ->get();
        return $data;
    }

    public function getDramaSerials(){
        $data=$this->model
            ->with('categories')
            ->where('serial_id','>',0)
            ->groupBy('serial_id')
            ->orderByRaw("RAND()")
            ->take(3)
            ->get();
        return $data;
    }

    public function getRecentVideosByCategoryName($categoryName){

        $data = $this->model->with('categories')
            ->whereHas('categories',function($q) use($categoryName){
                $q->where('categories.name','=',$categoryName);
            })
            ->orderBy('videos.id','desc')
            ->take(10)
            ->get()->toArray();
        return $data;
    }

    public function getPopularVideosByCategoryName($categoryName){

        $data = $this->model->with(['persons','productionHouse','categories'])
            ->whereHas('categories',function($q) use($categoryName){
                $q->where('categories.name','=',$categoryName);
            })
            ->leftJoin(DB::raw('(SELECT `general_playlists`.`video_id`,COUNT(*) AS popular
                                    FROM `general_playlists`
                                    GROUP BY general_playlists.`video_id` ORDER BY popular DESC) as popular'),'popular.video_id','=','videos.id')
            ->orderBy('popular','desc')
            ->take(10)
            ->get()->toArray();

        return $data;

    }

    public function getVideosByCategoryId($categoryId){

        $data = $this->model->with(['categories','productionHouse','generalPlaylists'])
            ->whereHas('categories',function($q) use($categoryId){
                $q->where('category_video.category_id','=',$categoryId);
            })
            ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
            ->simplePaginate(100);

        $res = $data->toArray();
        $data = $res['data'];
        $customData = [];
        foreach ($data as $item){
            $temp['id'] = $item['id'];
            $temp['slug'] = $item['slug'];
            $temp['title'] = $item['title'];
            $temp['description'] = $item['description'];
            $temp['duration'] = $item['duration'];
            $temp['thumbnail'] = $item['thumbnail'];
            $temp['quality'] = $item['quality'];
            $temp['likeCount'] = $item['favourite'];
            $temp['view'] = $item['general_playlists']?$item['general_playlists'][0]['view']:0;
            $temp['productionHouseName'] = $item['production_house']?$item['production_house']['name']:null;

            array_push($customData,$temp);
        }

        $res['data'] = $customData;
        return $res;
    }

    public function getVideosByCategoryName($categoryName){

        $data = $this->model->with(['categories','productionHouse','generalPlaylists'])
            ->whereHas('categories',function($q) use($categoryName){
                $q->where('categories.name','=',$categoryName);
            })
            ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
            ->simplePaginate(10);

        $res = $data->toArray();
        $data = $res['data'];
        $customData = [];
        foreach ($data as $item){
            $temp['id'] = $item['id'];
            $temp['slug'] = $item['slug'];
            $temp['title'] = $item['title'];
            $temp['description'] = $item['description'];
            $temp['duration'] = $item['duration'];
            $temp['thumbnail'] = $item['thumbnail'];
            $temp['quality'] = $item['quality'];
            $temp['likeCount'] = $item['favourite'];
            $temp['view'] = $item['general_playlists']?$item['general_playlists'][0]['view']:0;
            $temp['productionHouseName'] = $item['production_house']?$item['production_house']['name']:null;

            array_push($customData,$temp);
        }

        $res['data'] = $customData;
        return $res;
    }

    public function getVideosByCategorySlug($categorySlug){
        $data = $this->model->with(['categories','productionHouse','generalPlaylists'])
            ->whereHas('categories',function($q) use($categorySlug){
                $q->where('categories.slug','=',$categorySlug);
            })
            ->leftJoin(DB::raw('(SELECT `user_playlists`.`video_id`,COUNT(*) AS favourite
                                    FROM `user_playlists`
                                    WHERE user_playlists.`favourite`=1
                                    GROUP BY user_playlists.`video_id`) as favourites'),'favourites.video_id','=','videos.id')
            ->simplePaginate(100);

        $res = $data->toArray();
        $data = $res['data'];
        $customData = [];
        foreach ($data as $item){
            $temp['id'] = $item['id'];
            $temp['slug'] = $item['slug'];
            $temp['title'] = $item['title'];
            $temp['description'] = $item['description'];
            $temp['duration'] = $item['duration'];
            $temp['thumbnail'] = $item['thumbnail'];
            $temp['quality'] = $item['quality'];
            $temp['likeCount'] = $item['favourite'];
            $temp['view'] = $item['general_playlists']?$item['general_playlists'][0]['view']:0;
            $temp['productionHouseName'] = $item['production_house']?$item['production_house']['name']:null;

            array_push($customData,$temp);
        }

        $res['data'] = $customData;
        return $res;
    }

    public function getAllVideos(){

        return $this->model->get();
    }

}