<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 9/24/2016
 * Time: 4:55 PM
 */

namespace App\Api\V1\Repo;

use App\Model\Category;

class CategoryRepo
{

    private $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function getCategoryTree(){

        $categories = $this->model
                        ->with('subCategory')
                        ->where('parent_id','=',0)
                        ->where('name','!=','')
                        ->get()
                        ->toArray();
        return $categories;
    }

    public function getAllCategory(){
        $categories = $this->model->all();
        return $categories;
    }

}