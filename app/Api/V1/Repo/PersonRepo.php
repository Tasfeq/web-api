<?php namespace App\Api\V1\Repo;

use App\Model\Person;

class PersonRepo{
    /**
     * @var Person
     */
    private $model;

    public function __construct(Person $model)
    {
        $this->model = $model;
    }

    public function getPersonDetails($id){
        $res = $this->model->with('videos')
                ->where('persons.id',$id)
                ->first();
        if(!$res){
            return $res;
        }
        $res = $this->customizePersonDetailsWithType($res->toArray());
        return $res;
    }
    
    private function customizePersonDetailsWithType($data){
        $videos = $data['videos'];
        unset($data['videos']);
        $customizedData = array();
        foreach ($videos as $video){
            $index =  !empty($customizedData[$video['pivot']['role']])?count($customizedData[$video['pivot']['role']]):0;
            $customizedData[$video['pivot']['role']][$index] = $video;
        }
        $data['role'] = $customizedData;
        return $data;
    }
}