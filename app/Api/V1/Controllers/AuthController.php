<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use JWTAuth;
use Psy\Exception\ErrorException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Validator;
use Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;

class AuthController extends Controller{


    public function login(Request $request, $fbUserInfo=null)
    {
        $credentials = $request->only(['email', 'password']);
        if($fbUserInfo){
            $credentials = ['fbid'=>$fbUserInfo->fbid,'email'=>$fbUserInfo->email];
        }
        $validator = Validator::make($credentials, [
            'email' => 'required',
            'password' => 'required_without_all:fbid',
            'fbid'=>'required_without_all:password'
        ]);

        if($validator->fails()) {
            header('Access-Control-Allow-Origin: *');
            throw new ValidationHttpException($validator->errors()->all());
        }

        try {
            if(array_key_exists('fbid',$credentials)){
                $user = User::where('fbid',$credentials['fbid'])->first();
                if(!$user){
                    header('Access-Control-Allow-Origin: *');
                    return $this->response->errorUnauthorized();
                }else{
                    $token = JWTAuth::fromUser($user);
                }
            }
            elseif (! $token = JWTAuth::attempt($credentials)) {
                header('Access-Control-Allow-Origin: *');
                return $this->response->errorUnauthorized();
            }
        } catch (JWTException $e) {
            header('Access-Control-Allow-Origin: *');
            return $this->response->error('could_not_create_token', 500);
        }
        $userInfo = $this->getUserInfoFromToken($token);
        return response()->json($userInfo);
    }

    public function accountKitLogin(User $user){
        try {
            $token = JWTAuth::fromUser($user);
        } catch (JWTException $e) {
            header('Access-Control-Allow-Origin: *');
            return $this->response->error('could_not_create_token', 500);
        }
        $userInfo = $this->getUserInfoFromToken($token,$user);
        return response()->json($userInfo);
    }

    public function signup(Request $request)
    {
        $signupFields = ['name','email','password','password_confirmation','mobile','gender',
                        'country','city','birthday'];
        $hasToReleaseToken = true;

        $userData = $request->only($signupFields);

        try{
            $validator = Validator::make($userData, [
                'name'=>'required',
                'email'=>'required|unique:users',
                'password'=>'required|confirmed'
//                'mobile'=>'required|unique:users'
            ]);
        }catch(ErrorException $e){
            Log::info('Validator try catch',[$e]);
        }
        if($validator->fails()) {
            header('Access-Control-Allow-Origin: *');
//            return response()->json(['error' => $validator->errors(),'status'=>422]);
            throw new ValidationHttpException($validator->errors()->all());
        }

        User::unguard();

        $newUser = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
//            'mobile'=>$request->get('mobile'),
//            'gender'=>$request->get('gender'),
//            'birthday'=>$request->get('birthday'),
//            'country'=>$request->get('country'),
//            'city'=>$request->get('city')
        ];
        $user = User::create($newUser);
        User::reguard();

        if(!$user->id) {
            header('Access-Control-Allow-Origin: *');
            return $this->response->error('could_not_create_user', 500);
        }
        if($hasToReleaseToken) {
            return $this->login($request);
        }
    }

    public function postFbLogin(Request $request){

        Log::info('in fb login');
        $res = json_decode($this->getFbUserInfoFromAccessToken($request->access_token));
        Log::info('res'.json_encode($res));
        $userObj = ['fbid'=>$res->id,
                    'name'=>$res->name,
                    'image'=>$res->picture->data->url,
                    'access_token'=>$request->access_token,
                    'email'=>$res->email];
        try{
            $user = User::where('fbid',$userObj['fbid'])->orWhere('email',$res->email)->first();
            if($user){
                $user->update($userObj);
            }else{
                $user = User::create($userObj);
            }
            return $this->login($request,$user);
        }catch(ErrorException $e){
            Log::info('Validator try catch',[$e]);
        }


    }

    public function postFbAccountKitLogin(Request $request){
        Log::info('in fb account kit login');
        $res = json_decode($this->getFbUserFromAccountKitAccessToken($request->access_token));
        $mobile = str_replace('+','',$res->phone->number);
        try{
            $user = User::where('mobile',$mobile)->first();
            if($user){
                $user->update([
                    'mobile'=>$mobile,
                    'account_kit_access_token'=>$request->access_token
                ]);
            }else{
                $user = User::create([
                    'mobile'=>$mobile,
                    'account_kit_access_token'=>$request->access_token
                ]);
            }
            return $this->accountKitLogin($user);
        }catch(ErrorException $e){
            Log::info('Validator try catch',[$e]);
            return response()->json(['message'=>'user can not be created']);
        }
    }

    public function postResetPassword(Request $request){
        $credentials = $request->only(
            'old_password','password', 'password_confirmation'
        );
        $validator = Validator::make($credentials, [
            'old_password' => 'required|min:6',
            'password' => 'required|confirmed|min:6',
        ]);

        if($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        }
        $password=Auth::User()->password;
        if(!Hash::check($credentials['old_password'],$password))
        throw new \Dingo\Api\Exception\ResourceException('resource not found') ;
    }

    public function getUserInfo(Request $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function getUserInfoFromToken($token,$user=null){

        $user =  $user?$user:JWTAuth::toUser($token)->toArray();
        $user['token'] = $token;
        return $user;
    }

    public function updateUserInfo(Request $request){

        try{

            $file = $request->file('image');
            $fileName = '';
            if($file){
                $fileExt = $file->getClientOriginalExtension();
                $fileName = 'user_'.$request->id.'.'.$fileExt;

                if(!is_dir(public_path('/user_image'))){
                    mkdir(public_path('/user_image'),777);
                }
                $file->move(public_path('/user_image'),$fileName);
            }
            
            $user = User::find($request->id);

            $res = $user->update([
                'name' => $request->name,
                'email'=> (($user->email && $request->email)||(!$user->email && $request->email))?$request->email:$user->email,
                'gender'=> $request->gender,
                'image'=> $fileName?env('APP_URL').'user_image/'.$fileName:$user->image,
                'birthday'=>$request->birthday,
                'city'=>$request->city,
                'country'=>$request->country
            ]);

            return response()->json($user);

        }catch(ErrorException $e){
            return $this->response->errorNotFound();
        }
    }

    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);

        JWTAuth::invalidate($request->input('token'));
    }

    public function getFbUserInfoFromAccessToken($access_token){
        Log::info('Fb access token'.$access_token);
        try {
            $res = file_get_contents('https://graph.facebook.com/me?fields=name,email,picture&access_token=' . $access_token);
            return $res;
        } catch (RequestException $e) {
            $status = 'Login Invalid';
            return \Response::json(['status' => $status, 'user' => '']);
        }
    }

    public function getFbUserFromAccountKitAccessToken($access_token){
        try {
            $res = file_get_contents('https://graph.accountkit.com/v1.0/me?access_token='.$access_token);
            return $res;
        } catch (RequestException $e) {
            $status = 'Login Invalid';
            return \Response::json(['status' => $status, 'user' => '']);
        }
    }

    public function verificationCode($userId){
        $user = User::find($userId);

    }

    private function generateVerificationCode(){
        return rand('100000','999999');

    }

    public function refreshToken(){
        return response()->json(['message'=>'refreshed']);
    }

    public function recovery(Request $request)
    {

        $validator = Validator::make($request->only('phone'), [
            'phone'=> 'required|exists:users,phone'
        ]);

        if($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        }
        $user=$this->userRepo->getUserByPhone($request->input('phone'));
        $response = Password::sendResetLink(['email'=>$user->email], function (Message $message) {
            $message->subject(Config::get('boilerplate.recovery_email_subject'));
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->response->noContent();
            case Password::INVALID_USER:
                return $this->response->errorNotFound();
        }
    }

    public function reset(Request $request)
    {
        $credentials = $request->only(
            'password', 'password_confirmation','token','phone'
        );

        $validator = Validator::make($credentials, [
            'token' => 'required',
            'phone' => 'required|exists:users,phone',
            'password' => 'required|confirmed|min:6',
        ]);

        if($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        }
        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                if(Config::get('boilerplate.reset_token_release')) {
                    return $this->login($request);
                }
                return $this->response->noContent();

            default:
                return $this->response->error('could_not_reset_password', 500);
        }
    }

}