<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Repo\VideoRepo;
use App\Api\V1\Repo\CategoryRepo;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Comment;
use Psy\Exception\ErrorException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller{
    /**
     * @var VideoRepo
     */
    private $videoRepo;
    private $categoryRepo;

    public function __construct(VideoRepo $videoRepo,CategoryRepo $categoryRepo)
    {
        $this->videoRepo = $videoRepo;
        $this->categoryRepo= $categoryRepo;
    }

    public function index(){

    }

    public function show($id){

        try{
            if(JWTAuth::getToken()){

                $res = $this->getAuthenticatedUser();
                if($res[1]>200){
                    $user = null;
                }else{
                    $user = $res[0];
                }

            }else{
                $user = null;
            }
            $data = $this->videoRepo->getVideoDetails($id,$user);
            if($data){
                $result=$this->minifyJsonData($data);
                return response()->json($result);
            }else {
                header('Access-Control-Allow-Origin: *');
                $this->response()->errorNotFound('Video not found');
            }

        }catch (ErrorException $e){
            header('Access-Control-Allow-Origin: *');
            return $e;
        }
    }
    
    public function showBySlug($slug){
        try{
            if(JWTAuth::getToken()){

                $res = $this->getAuthenticatedUser();
                if($res[1]>200){
                    $user = null;
                }else{
                    $user = $res[0];
                }

            }else{
                $user = null;
            }
            $data = $this->videoRepo->getVideoDetailsBySlug($slug,$user);
            if($data){
                $result=$this->minifyJsonData($data);
                return response()->json($result);
            }else {
                header('Access-Control-Allow-Origin: *');
                $this->response()->errorNotFound('Video not found');
            }

        }catch (ErrorException $e){
            header('Access-Control-Allow-Origin: *');
            return $e;
        }
    }

    public function getSimilarVideos($id){
        try{
            $data = $this->videoRepo->getSimilarVideos($id);
            return $data?response()->json($data):$this->response()->errorNotFound('No similar video found');
        }catch (ErrorException $e){
            return $e;
        }

    }

    public function getRecommendedVideos($id=null){
        try{
            $data = $this->videoRepo->getRecommendedVideos($id);
            return $data?response()->json($data):$this->response()->errorNotFound('No recommended video found');
        }catch (ErrorException $e){
            return $e;
        }
    }
    

    public function videoActionLog($action, $videoId,Request $request){

        $time=null;

        $time=$request->has('time')?$request->get('time'):null;
        $time = Carbon::createFromFormat('H:i:s',gmdate("H:i:s", $time))->toTimeString();
        $user = $this->user();

        if($user){
            if($action=="play"){
                $this->videoRepo->insertIntoUserPlaylist($videoId,$user,$inc=1,$time);
            }elseif ($action=="pause" || $action=="seek" || $action=="updatetime"){
                $this->videoRepo->insertIntoUserPlaylist($videoId,$user,$inc=0,$time);
            }elseif ($action=="end"){
                $this->videoRepo->insertIntoUserPlaylist($videoId,$user,$inc=0,$time);
            }
        }
        if($action=="play") {
            $this->videoRepo->insertIntoGeneralPlaylist($videoId);
        }

        if($action!="updatetime"){
            $requestData=$request->all();
            $info=array('video_id'=>$videoId,'user_id'=>$user,'action'=>$action);
            $logData=$this->processLogData($info,$requestData);

            $this->videoRepo->insertIntoVideoWatchLog($logData);
        }

    }


    public function videoLike($videoId){

        $userId=$this->user();
        //if()
        $playlistData=$this->getUserPlaylistData($videoId,$userId);

        if($playlistData){
            if($playlistData->favourite==1){
                $favourite=0;
            }else{
                $favourite=1;
            }
        }else{
            $favourite=1;
        }

        $data['id']=$playlistData?$playlistData->id:null;
        $data['favourite']=$favourite;
        $data['user_id'] = $userId;
        $data['video_id'] = $videoId;
        $this->updateUserPlaylist($data);
        return response()->json(["Successfully Done"]);
    }

    public function getUserPlaylistData($videoId,$userID){

        $playlist = DB::table('user_playlists')
                        ->where('user_id', '=',$userID)
                        ->where('video_id', '=',$videoId)
                        ->first();

        return $playlist;
    }

    public function updateUserPlaylist($data){

        if($data['id']){
            DB::table('user_playlists')
                ->where('id', $data['id'])
                ->update(['favourite' => $data['favourite']]);
        }else{
            DB::table('user_playlists')->insert(['video_id'=>$data['video_id'],
                                                 'user_id'=>$data['user_id'],
                                                 'favourite' => $data['favourite']]);
        }

    }

    /*
     * @todo move these function to comment repo
     */

    public function categoryVideos($categoryName){

        $videos=$this->videoRepo->getVideosByCategoryName($categoryName);
        return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');
    }

    public function recentVideos(){

        $videos=$this->videoRepo->getRecentlyUploadedVideos();
        return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');
    }

    public function popularDrama(){

        $popularDramas=$this->videoRepo->getPopularDrama();
        return $popularDramas?response()->json($popularDramas):$this->response()->errorNotFound('No Videos Found');

    }

    public function popularMusic(){

        $popularMusics=$this->videoRepo->getPopularMusic();
        return $popularMusics?response()->json($popularMusics):$this->response()->errorNotFound('No Videos Found');

    }

    public function getTrendingVideo(){
        $trendingVideos=$this->videoRepo->getTrendingVideo();
        return $trendingVideos?response()->json($trendingVideos):$this->response()->errorNotFound('No Videos Found');
    }

    public function getRecentMovies(){
        $recentMovies=$this->videoRepo->getRecentMovies();
        return $recentMovies?response()->json($recentMovies):$this->response()->errorNotFound('No Videos Found');
    }

    public function getRecentDrama(){
        $recentDrama = $this->videoRepo->getRecentVideoDataByCategory(4);
        return $recentDrama?response()->json($recentDrama):$this->response()->errorNotFound('No Videos Found');

    }

    public function getDramaSerials(){
        $data = $this->videoRepo->getDramaSerials();
        return $data?response()->json($data):$this->response()->errorNotFound('No Videos Found');
    }

    public function similarVideos($videoId){

        $data=$this->videoRepo->getVideoDataById($videoId);
        $personArray=$this->getPersonsFromVideoData($data);
        return $this->sendSearchQueryToElasticSearch($personArray);
    }
    
    public function similarVideosBySlug($slug){
        $data=$this->videoRepo->getVideoBySlug($slug);
        $personArray=$this->getPersonsFromVideoData($data);
        return $this->sendSearchQueryToElasticSearch($personArray);
    }

    public function getPersonsFromVideoData($videoData){

        $persons=array();
        $personRoles=$videoData['role'];
        foreach($personRoles as $key=>$person){
            array_push($persons,$person[0]['name']);
        }
        return $persons;

    }

    public function sendSearchQueryToElasticSearch($query){

        $queryString='';
        for($i=0;$i<sizeof($query);$i++){
            $queryString .= $query[$i];
            if($i!=sizeof($query)-1){
                $queryString .= ' ';
            }
        }
        $url=env('SEARCH_API')."search/".rawurlencode($queryString) ;
        
        $res = json_decode(file_get_contents($url));
        if($res->hits->total){
            return response()->json($res->hits->hits);
        }else{
            header('Access-Control-Allow-Origin: *');
            return $this->response()->errorNotFound('Search result empty');
        }
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return array('token_expired', $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return array('token_invalid', $e->getStatusCode());

        } catch (JWTException $e) {

            return array('token_absent', $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return array($user->id,200);
    }

    public function getRecentVideosByCategoryName($categoryName){

        $videos=$this->videoRepo->getRecentVideosByCategoryName($categoryName);
        return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');

    }

    public function getPopularVideosByCategoryName($categoryName){

        $videos=$this->videoRepo->getPopularVideosByCategoryName($categoryName);
        return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');
    }

    public function getVideosByCategoryIdorCategoryNameorCategorySlug(Request $request){

        if($request->has('categoryId')){
            $categoryId=$request->input('categoryId');
            $videos=$this->videoRepo->getVideosByCategoryId($categoryId);
            return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');
        }elseif($request->has('categoryName')){
            $categoryName=$request->input('categoryName');
            $videos=$this->videoRepo->getVideosByCategoryName($categoryName);
            return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');
        }elseif($request->has('categorySlug')){
            $categorySlug=$request->input('categorySlug');
            $videos=$this->videoRepo->getVideosByCategorySlug($categorySlug);
            return $videos?response()->json($videos):$this->response()->errorNotFound('No Videos Found');
        }else{
            $this->response()->errorNotFound('Invalid Request! Only valid Parameters are categoryId and categoryName');
        }

    }
    

    public function getHomepageBanners(){

        $banners = Banner::with('videos')->get()->toArray();
        return $banners?response()->json($banners):$this->response()->errorNotFound('No Banners Found');
    }

    public function processLogData($info,$requestData){

        $processedData=array(
            'browser'=>!empty($requestData['browser'])?$requestData['browser']:null,
            'browser_version'=>!empty($requestData['browser_version'])?$requestData['browser_version']:null,
            'screen_size'=>!empty($requestData['screen_size'])?$requestData['screen_size']:null,
            'mobile'=>!empty($requestData['mobile'])?($requestData['mobile']?1:0):0,
            'platform'=>!empty($requestData['platform'])?$requestData['platform']:null,
            'flash'=>!empty($requestData['flash'])?$requestData['flash']:null,
            'os'=>!empty($requestData['os'])?$requestData['os']:null,
            'os_version'=>!empty($requestData['os_version'])?$requestData['os_version']:null,
            'cookie'=>!empty($requestData['cookie'])?($requestData['cookie']?1:0):0,
            'video_id'=>$info['video_id'],
            'action'=>$info['action'],
            'time'=>!empty($requestData['time'])?Carbon::createFromFormat('H:i:s',gmdate("H:i:s", $requestData['time']))->toTimeString():null,
            'user_id'=>$info['user_id']
        );

        return $processedData;

    }

    public function setVideoSlugs(){
        $data=$this->videoRepo->getAllVideos();
        foreach($data as $key=>$video){
            $slug = preg_replace('/[^a-z0-9\']/i',' ', strtolower(trim($video->title)));
            $slug = preg_replace('/\s+/','-', $slug);
            DB::table('videos')
                ->where('id', $video->id)
                ->update(array('slug' => $slug));
        }
        return $data?response()->json($data):$this->response()->errorNotFound('No slugs Found');
    }

    public function bulkVideoJson(VideoRepo $videoRepo){
        set_time_limit ( 3600 );
        $elasticSearch=new ElasticSearchController($videoRepo);
        $dataArr=array();
        $data=$this->videoRepo->getAllVideos();
        foreach($data as $key=>$video){
            $videoData=$this->videoRepo->getVideoDataById($video->id);
            $customizedData=$elasticSearch->customizeDataForSearchAPI($videoData);
            $jsonData=json_encode($customizedData);
            //Log::info($jsonData);
            $elasticSearch->createVideoJson($jsonData,$video->id);
            $result = $elasticSearch->sendDataToElasticSearch($jsonData);

            $dataArr[$key]['data']=$video;
        }

        return $dataArr?response()->json($dataArr):$this->response()->errorNotFound('Sorry Man');
    }

    public function minifyJsonData($data){

        $removeProductionHouseKeys = array('description', 'image','created_at','updated_at','post_id');
        foreach($removeProductionHouseKeys as $key) {
            unset($data['production_house'][$key]);
        }

        for($i=0;$i<=sizeof($data['categories']);$i++){
            unset($data['categories'][$i]['pivot']);
            unset($data['categories'][$i]['created_at']);
            unset($data['categories'][$i]['updated_at']);
            unset($data['categories'][$i]['slug']);
        }

        $roleDataFilters=array('singer','writer','author','artist','cinematographer','screenplay','director','producer');
        $removeRoleKeys = array('pivot', 'created_at','updated_at','image','description','birth_date','death_date','post_id');
        for($k=0;$k<sizeof($roleDataFilters);$k++){
            if(array_key_exists($roleDataFilters[$k],$data['role'])) {
                for ($i=0;$i<sizeof($data['role'][$roleDataFilters[$k]]);$i++) {
                    foreach($removeRoleKeys as $key) {
                        unset($data['role'][$roleDataFilters[$k]][$i][$key]);
                    }
                }
                //dd($data['role'][$roleDataFilters[$k]][0]['pivot']);
                //dd(sizeof($data['role'][$roleDataFilters[$k]]));
            }
        }

        $removeVideoKeys = array('restricted','updated_at','link','revenue_share','territory','type');
        foreach($removeVideoKeys as $key) {
            unset($data[$key]);
        }
        return $data;
    }
}