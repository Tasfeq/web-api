<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Comment;
use Illuminate\Http\Request;
use Psy\Exception\ErrorException;

class CommentController extends Controller{

    /**
     * @var Comment
     */
    private $comment;

    public function __construct(Comment $comment)
    {

        $this->comment = $comment;
    }

    public function getVideoComment($videoId){
        try{
            $comments = Comment::with('user')
                ->where('video_id',$videoId)
                ->where('status',"Published")
                ->orderBy('created_at','desc')
                ->get();
            if(count($comments)){
                return response()->json($comments);
            }else {
                header('Access-Control-Allow-Origin: *');
                $this->response()->errorNotFound('no comment found');
            }
        }catch (ErrorException $e){
            return $e;
        }
    }

    /**
     * @param $videoId
     * @param Request $request
     * @return ErrorException|\Exception|\Illuminate\Http\JsonResponse
     */
    public function postVideoComment($videoId, Request $request){

        try{
            $text = $request->get('comment');
            $userId=$this->user();
            $this->comment->user_id=$userId;
            $this->comment->video_id=$videoId;
            $this->comment->parent_id=null;
            $this->comment->description=$text;
            $this->comment->status="Published";
            $this->comment->save();

            return response()->json(['Comment saved']);
        }catch (ErrorException $e){
            return $e;
        }

    }

    public function updateVideoComment($id, Request $request){
        try{
            $text = $request->get('comment');
            $userId=$this->user();
            $commentModel = $this->comment->find($id);
            if($userId == $commentModel->user_id){
                $commentModel->description = $text;
                $res = $commentModel->save();
                return $res?response()->json(['Comment updated'],200):response()->json(['Not modified'],304);
            }else{
                return response()->json(["This comment doesn't belong to you"], 403);
            }

        }catch (ErrorException $e){
            return $e;
        }
    }

    public function deleteVideoComment($commentId){

        try{
            $comment = $this->comment->find($commentId);
            $comment->delete();
            return response()->json(['Comment deleted']);
        }catch (ErrorException $e){
            return $e;
        }

    }
}