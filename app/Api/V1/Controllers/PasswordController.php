<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function postPasswordResetEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject('Password reset link');
            $message->sender('cubex.com.bd@gmail.com');
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return response()->json(['status'=>trans($response)]);
            case Password::INVALID_USER:
                return response()->json(['email' => trans($response)]);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        if($this->checkPasswordResetTokenIsExpired($request['token'])){
            $userInfo = $this->getUserFromPasswordResetToken($request['token']);
            $this->resetPassword($userInfo,$request['password']);
            return response()->json(['status'=>'password successfully reseted']);
        }else{
            return response()->json(['error'=>'token expired']);
        }


    }

    protected function resetPassword( $user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
    }

    private function getUserFromPasswordResetToken($token){
        $userId = User::join('password_resets','password_resets.email','=','users.email')
                      ->where('password_resets.token',$token)
                      ->pluck('id');
        return User::find($userId);
    }

    private function checkPasswordResetTokenIsExpired($token){
        //expiry time in minute
        $expires = 60;

        $passwordResetObj = DB::table('password_resets')->where('token',$token)->first();

        if(!$passwordResetObj){
            return false;
        }

        $expires = strtotime($passwordResetObj->created_at)+$expires*60;
        $currentTime = time();

        return $currentTime>$expires?true:false;
    }
}
