<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Api\V1\Repo\VideoRepo;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Psy\Exception\ErrorException;

class ElasticSearchController extends Controller
{
    /**
     *
     */
    private $videoRepo;

    public function __construct(VideoRepo $videoRepo)
    {
        $this->videoRepo = $videoRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function wp_videoData($videoId){

        Log::info('from wordpress '.$videoId);
        $videoData=$this->videoRepo->getVideoDataById($videoId);
        $customizedData=$this->customizeDataForSearchAPI($videoData);
        $jsonData=json_encode($customizedData);
        Log::info($jsonData);
        $this->createVideoJson($jsonData,$videoId);
        //return $videoData?$videoData:$this->response->errorNotFound();
        $res = $this->sendDataToElasticSearch($jsonData);

        dd('file created');
    }

    public function customizeDataForSearchAPI($data){

        $tagJson=$data['tag'];
        $qualityJson=$data['quality'];
        unset($data['tag']);
        unset($data['quality']);
        $tagData=json_decode($tagJson);
        $qualityData=json_decode($qualityJson);
        $data['tag']=$tagData;
        $data['quality']=$qualityData;

        $removeProductionHouseKeys = array('description', 'image','created_at','updated_at');
        foreach($removeProductionHouseKeys as $key) {
            unset($data['production_house'][$key]);
        }

        $removeVideoKeys = array('restricted','updated_at','production_house_id','quality');
        foreach($removeVideoKeys as $key) {
            unset($data[$key]);
        }
        return $data;
    }

    public function createVideoJson($jsonData,$id){

        $old_directory = public_path();
        $new_directory = public_path().'/Video_Json/';

        if (!is_dir($new_directory)) {
            mkdir($new_directory, 0777, true);
        }

        $fp = fopen('videoJson_'.$id.'.json', 'w');
        fwrite($fp, $jsonData);
        fclose($fp);

        $file_name='videoJson_'.$id.'.json';
        rename($old_directory.'/'.$file_name, $new_directory.'/'.$file_name);
    }

    public function sendDataToElasticSearch($data){

        try{
            $ch = curl_init(env('SEARCH_API'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $result = curl_exec($ch);
            if(curl_errno($ch)){
                return 'Request Error:' . curl_error($ch);
            }
            curl_close($ch);
            return $result;
        }catch (ErrorException $e){
            return $e;
        }
    }

    public function getSearchResult(Request $request){

        Log::info($request->get('query'));
        if($request->has('query') && strlen($request->get('query'))>0){
            $url = env('SEARCH_API').'search/'.rawurlencode($request->get('query'));
            $res = json_decode(file_get_contents($url));
            if($res->hits->total){
                return response()->json($res->hits->hits);
            }else{
                header('Access-Control-Allow-Origin: *');
                return $this->response()->errorNotFound('Search result empty');
            }
        }else{
            header('Access-Control-Allow-Origin: *');
            return $this->response()->errorNotFound('Not a valid query');
        }
    }

}
