<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Repo\CategoryRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepo
     */
    private $categoryRepo;

    public function __construct(CategoryRepo $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function getVideoCategoryTree(){

        $category=$this->categoryRepo->getCategoryTree();
        return $category?response()->json($category):$this->response()->errorNotFound('No Categories Found');
    }

    public function setCategorySlug(){
        $categories = $this->categoryRepo->getAllCategory();

        foreach($categories as $key=>$category){
            $slug = preg_replace('/[^a-z0-9\']/i',' ', strtolower(trim($category->name)));
            $slug = preg_replace('/\s+/','-', $slug);
            DB::table('categories')
                ->where('id', $category->id)
                ->update(array('slug' => $slug));
        }
        return $categories?response()->json($categories):$this->response()->errorNotFound('No slugs Found');
    }
}
