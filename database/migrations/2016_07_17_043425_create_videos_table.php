<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('territory');
            $table->integer('point');
            $table->string('duration');
            $table->string('link');
            $table->decimal('revenue_share',8,2);
            $table->string('thumbnail');
            $table->boolean('restricted')->default(false);
            $table->integer('post_id');
            $table->integer('production_house_id');
            $table->integer('serial_id');
            $table->text('tag');
            $table->text('quality');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
