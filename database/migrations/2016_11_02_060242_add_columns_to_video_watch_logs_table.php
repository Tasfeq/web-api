<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToVideoWatchLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_watch_logs',function (Blueprint $table){
            $table->string('os',100)->nullable();
            $table->string('os_version',20)->nullable();
            $table->boolean('cookie')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_watch_logs', function (Blueprint $table) {
            $table->dropColumn(['os','os_version','cookie']);
        });
    }
}
