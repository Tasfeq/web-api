<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToVideoWatchLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_watch_logs', function (Blueprint $table) {
            $table->string('browser')->nullable();
            $table->string('browser_version')->nullable();
            $table->string('screen_size')->nullable();
            $table->boolean('mobile');
            $table->enum('platform', ['web', 'app']);
            $table->string('flash')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_watch_logs', function (Blueprint $table) {
            $table->dropColumn('browser');
            $table->dropColumn('screen_size');
            $table->dropColumn('mobile');
            $table->dropColumn('platform');
            $table->dropColumn('flash');
        });
    }
}
